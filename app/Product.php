<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'price', 'quantity', 'description', 'picture_id', 'slug', 'user_id'
    ];

    protected $table = 'products';

    public function picture()
    {
        return $this->hasMany('App\Picture');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function card()
    {
        return $this->hasMany(Card::class);
    }
}
