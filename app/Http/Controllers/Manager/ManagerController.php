<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Mananger\ManagerRequest;



class ManagerController extends Controller
{
//    use AuthenticatesUsers;
//    public function create(ManagerRequest $request)
//    {
//
//        $credentials = [
//            'email' => $request->email,
//            'password' => $request->password,
//        ];
//        $attempt = $this->guard()->attempt($credentials);
//
//        if ($attempt) {
//            $this->attemptLogin($request);
//            return view('manager.home');
//        }
//    }

    public function logout()
    {
        Auth::logout();
        return view('user.login');

    }
}
