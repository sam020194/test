<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdminRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AdminController extends Controller
{
//    use AuthenticatesUsers;
//    public function create(AdminRequest $request)
//    {
//
//        $credentials = [
//            'email' => $request->email,
//            'password' => $request->password,
//        ];
//        $attempt = $this->guard()->attempt($credentials);
//
//        if ($attempt) {
//            $this->attemptLogin($request);
//            return view('admin.home');
//        }
//    }

    public function logout()
    {
        Auth::logout();
        return view('user.login');

    }
}
