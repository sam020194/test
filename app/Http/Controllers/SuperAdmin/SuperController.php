<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SuperAdmin\CreateRequest;
use App\Product;
use App\Repositories\Interfaces\UserRepositoryInterface;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\StaticAnalysis\HappyPath\AssertIsArray\consume;
use App\Repositories\UserRepository;



class SuperController extends Controller
{

    public $userRepo;

    /**
     * SuperController constructor.
     * @param UserRepositoryInterface $userModelObject
     */
    public function __construct(UserRepositoryInterface $userModelObject)
    {
        $this->userRepo = $userModelObject;
    }

    /**
     * @return in  all user
     */
    public function users()
    {
        $users = $this->userRepo->getAll();

        return view('super_admin.users', ['users' => $users, 'user' => \auth()->user()]);
    }

    /**
     * @param $id
     * delete in user
     * @return Redirect users and massage
     */
    public function destroy($id)
    {
        $this->userRepo->delete($id);
        return redirect()->route('users')->with('success','User deleted successfully');
    }

    /**
     * @return super_admin add_user
     */
    public function addUser()
    {
        return view('super_admin.add_user', ['user' => \auth()->user()]);
    }

    /**
     * @param CreateRequest $request
     * create user to Database
     * @return Redirect super_admin add_user and massage
     */
    public function createUser(CreateRequest $request)
    {
        $userData = $request->except('_token');
        $userData['password'] = Hash::make($userData['password']);

        $this->userRepo->create($userData);
        return redirect()->route('addUser')->with('success','User added successfully');
    }

    /**
     * @return all products users
     */
    public function products()
    {
        $products=$this->userRepo->allProducts();
        return view('super_admin.products', ['products'=>$products, 'user' => \auth()->user()]);
    }

    /**
     * @param $id
     * delete product
     * @return Redirect to super_admin.products and massage
     */
    public function productDestroy($id)
    {
        $this->userRepo->productDestroy($id);
        return redirect()->route('products')->with('success','Product deleted successfully');
    }
}
