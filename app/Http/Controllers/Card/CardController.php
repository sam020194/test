<?php

namespace App\Http\Controllers\Card;

use App\BuyProduct;
use App\Card;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CardController extends Controller
{
    public function addToCart($id)
    {
        $product=Product::where('id', '=', $id)->first();
        $userId=\auth()->id();
        $card = Card::where("user_id", '==', $userId)->where("product_id", $id)->get();


        if (count($card) == 0){
           $obj = new Card();
           $obj->user_id = $userId;
           $obj->product_id = $id;
           $obj->name = $product->name;
           $obj->price = $product->price;
           $obj->quantity = 1;
           $obj->save();
        }

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function show()
    {
        $userId = \auth()->user()->id;
//        dd($userId);
        $card = Card::all()->where('user_id', '==', $userId);
//        dd($card);
        return view('user.card', ['card' => $card, 'user' => \auth()->user()]);
    }

    public  function destroy($id)
    {
//        dd($id);
        DB::table('card')->where('id', $id)->delete();
        return redirect()->route('card')->with('success','Product deleted successfully');
    }

    public function stripe()
    {
        return view('user.stripe');
    }

    public function charge(){
        $userId=\auth()->id();
        $cards=Card::where('user_id', $userId)->get();
//        dd($card);
        foreach ($cards as $card){
//            dd($card);
//            if (isset($card[$userId])){
//                dd(555);
//            };
//            dd(6999);
//            $byyProduct=BuyProduct::create([
//                'product_id' => $card->product_id,
//                'user_id' => $card->user_id,
//                'name' => $card->name,
//            ]);
//            for ()
        }
    }

}
