<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\User\UserRegisterRequest;


class RegisterController extends Controller
{
    /**
     * store user registration
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('user.register');
    }


    /**
     * registration in user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $userData = $request->except('_token');
        $userData['password'] = Hash::make($userData['password']);


        $user = User::create($userData);
        return view('user.login');

    }
}
