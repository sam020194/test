<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserProductRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * @return auth user profile
     */
    public function profile()
    {
        $user = Auth::user();
        return view('user.home', compact('user'));
    }
}
