<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\User\UserLoginRequest;


class LoginController extends Controller
{

    /**
     * login in user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = \auth()->user();
            return view('user.home', compact('user'));
        } else{
            return back();
        }
    }

    /**
     * user in logout
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function logout()
    {
        Auth::logout();
        return view('user.login');

    }
}



