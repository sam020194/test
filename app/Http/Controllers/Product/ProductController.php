<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserProductRequest;
use App\Picture;
use App\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Services\ImageUpload;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\StaticAnalysis\HappyPath\AssertNotInstanceOf\A;

class ProductController extends Controller
{
    /**
     * ProductController constructor.
     * @param ProductRepositoryInterface $productModelObject
     * @param ImageUpload $imageService
     */
    public $productRepo, $imageService;

    public function __construct(ProductRepositoryInterface $productModelObject, ImageUpload $imageService)
    {
        $this->productRepo = $productModelObject;
        $this->imageService = $imageService;
    }

    /**
     * @param UserProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createProduct(UserProductRequest $request)
    {
        $data = [
            'user_id' => auth()->user()->id,
            'name' => $request->product_name,
            'price' => $request->product_price,
            'quantity' => $request->available_quantity,
            'description' => $request->product_description,
        ];
        $product = $this->productRepo->createProduct($data);
        if($request->hasFile('picture')){
            $this->imageService->addProductImage($request->file('picture'), $product->id);
        }

        return redirect()->route('addProduct')->with('success', 'Your product has been created!');
    }

    /**
     * @return user.add_product blade
     */
    public function addProduct()
    {
        return view('user.add_product', [ 'user' => \auth()->user()]);
    }

    /**
     * @param Request $request
     * @return \user.my_product
     */
    public function myProduct(Request $request)
    {
        $products = $this->productRepo->myProducts();
        return view('user.my_product', ['products' => $products, 'user' => \auth()->user()]);
    }

    /**
     * @param $id
     * delete my product
     * @return \user.my_product
     */
    public function destroy($id)
    {
        $this->productRepo->deleteProduct($id);
        return redirect()->route('myProduct')->with('success','Product deleted successfully');
    }

    /**
     *
     * @return users all products
     */
    public  function  allProduct()
    {
        $products = $this->productRepo->allProduct();
        return view('user.all_product', ['products' => $products,  'user' => \auth()->user()]);
    }


}
/**
 * @param $params
 *
 * @return mixed
 * @author sergeymirakyan@gmail.com
 */
