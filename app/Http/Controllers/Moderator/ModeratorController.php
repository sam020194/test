<?php

namespace App\Http\Controllers\Moderator;

use App\Http\Controllers\Controller;
use App\Http\Requests\Moderator\ModeratorRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\AdminRequest;
use Illuminate\Support\Facades\Auth;


class ModeratorController extends Controller
{
//    use AuthenticatesUsers;
//    public function create(ModeratorRequest $request)
//    {
//
//        $credentials = [
//            'email' => $request->email,
//            'password' => $request->password,
//        ];
//        $attempt = $this->guard()->attempt($credentials);
//
//        if ($attempt) {
//            $this->attemptLogin($request);
//            return view('moderator.home');
//        }
//    }

    public function logout()
    {
        Auth::logout();
        return view('user.login');

    }
}
