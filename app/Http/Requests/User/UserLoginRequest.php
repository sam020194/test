<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'  => 'required|unique:users,email',
            'password' => 'required|min:6|max:8',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'A email is required',
            'password.required' => 'A required is required',
            'password.min:6.max:8' => 'Password min-6 max-8 ',
        ];
    }
}
