<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string',
            'surname'  => 'required|required',
            'age'  => 'required|integer',
            'email'  => 'required|unique:users,email',
            'password' => 'required|min:6|max:8',
            'phone'=>'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'name.string' => 'A name is need to be string',
            'surname.required' => 'A surname is required',
            'surname.string' => 'A surname is need to be string',
            'age.required' => 'A age is required',
            'age.integer' => 'A age is need to be number',
            'email.required' => 'A email is required',
            'password.max:8' => 'Password min-6 max-8 ',
            'phone.required' => 'A phone is required',
            'phone.integer' => 'A phone is need to be number',
        ];
    }
}
