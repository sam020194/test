<?php


namespace App\Repositories;
use App\Repositories\Interfaces\ProductRepositoryInterface;

use App\Product;

class ProductRepository implements ProductRepositoryInterface
{
    protected $model;

    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    public function myProducts()
    {
        return $this->model->where('user_id', \auth()->user()->id)->get();
    }

    public function deleteProduct($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    public function allProduct()
    {
        return $this->model->where('user_id', '!=', auth()->id())->get();
    }


    public function createProduct($data)
    {
        return $this->model->create($data);
    }
}
