<?php


namespace App\Repositories\Interfaces;


interface ProductRepositoryInterface
{
    public function myProducts();

    public function deleteProduct($id);

    public function allProduct();

    public function createProduct($request);

}
