<?php


namespace App\Repositories\Interfaces;

use App\User;

interface UserRepositoryInterface
{
    public function getAll();

    public function delete($id);

    public function create($userData);

    public function allProducts();

    public function productDestroy($id);
}
