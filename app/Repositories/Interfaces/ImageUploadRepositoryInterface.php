<?php


namespace App\Repositories\Interfaces;


interface ImageUploadRepositoryInterface
{
    public function createImage(array $data);
}
