<?php
namespace  App\Repositories;

use App\Product;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function getAll()
    {
        return $this->model->paginate(20);

    }

    public function delete($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    public function create($userData)
    {
        return $this->model = User::created($userData);
    }

    public function allProducts()
    {
        return $this->model = Product::all();
    }

    public function productDestroy($id)
    {
        return $this->model = DB::table('products')->where('id', $id)->delete();
    }
}
//        DB::table('products')->where('id', $id)->delete();
