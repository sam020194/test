<?php


namespace App\Repositories;
use App\Picture;
use App\Repositories\Interfaces\ ImageUploadRepositoryInterface;


class ImageUploadRepository implements ImageUploadRepositoryInterface
{
    protected  $image;

    public function  __construct(Picture $picture)
    {
        $this->image = $picture;
    }

    public function createImage(array $data)
    {
        return $this->image->create($data);
    }
}
