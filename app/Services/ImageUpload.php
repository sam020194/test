<?php


namespace App\Services;

use App\Picture;
use App\Repositories\Interfaces\ImageUploadRepositoryInterface;

class ImageUpload
{
    public $imageRepo;

    public function __construct(ImageUploadRepositoryInterface $imageModelObject)
    {
        $this->imageRepo = $imageModelObject;
    }

    public function addProductImage($file, $productId)
    {
        $destinationPath = public_path('picture/');
        $image = date('YmdHis') . "." . $file->getClientOriginalExtension();
        $file->move($destinationPath, $image);

        $data = [
            'product_id' => $productId,
            'picture' => $image,
        ];
        $this->imageRepo->createImage($data);
        return $image;
    }
}
