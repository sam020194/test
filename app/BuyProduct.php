<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyProduct extends Model
{
    protected $fillable = [
        'name', 'price', 'quantity', 'product_id', 'user_id'
    ];

    protected $table = 'buy_product';
}
