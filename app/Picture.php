<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $fillable = [
      'picture_id', 'picture', 'product_id'
    ];
    protected $table = 'pictures';
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
