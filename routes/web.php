<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('user/login', function () {
    return view('user.login');
})->name('user.login.page');

Route::get('user/register', function () {
    return view('user.register');
});


Route::group(['prefix'=>'super_admin', 'namespace'=>'SuperAdmin'], function (){

    Route::get('/sup', 'SuperController@sup')->name('sup');
    Route::get('/super_admin.logout', 'SuperController@logout')->name('super_admin.logout');
    Route::get('/users', 'SuperController@users')->name('users');
    Route::delete('/user.destroy/{id}', 'SuperController@destroy')->name('user.destroy');
    Route::get('/addUser', 'SuperController@addUser')->name('addUser');
    Route::get('/products', 'SuperController@products')->name('products');
    Route::delete('/destroy/{id}', 'SuperController@productDestroy')->name('destroy');
    Route::post('createUser', 'SuperController@createUser')->name('createUser');

});


Route::group(['prefix'=>'user', 'namespace'=>'User'], function (){

    Route::post('/userCreate', 'RegisterController@create')->name('userCreate');
    Route::post('/store', 'LoginController@store')->name('user.store');
    Route::get('/logout', 'LoginController@logout')->name('user.logout');
    Route::get('/user.register', 'RegisterController@index')->name('user.register');
    Route::get('/profile', 'UserController@profile')->name('profile')->middleware('auth');

});

Route::group(['prefix'=>'user', 'namespace'=>'Product'], function (){
    Route::post('/createProduct', 'ProductController@createProduct')->name('createProduct');
    Route::get('/addProduct', 'ProductController@addProduct')->name('addProduct')->middleware('auth');
    Route::get('/myProduct', 'ProductController@myProduct')->name('myProduct');
    Route::delete('/product.destroy/{id}', 'ProductController@destroy')->name('product.destroy');
    Route::get('/allProduct', 'ProductController@allProduct')->name('allProduct');
//    Route::get('add_to_cart/{id}', 'ProductController@addToCart')->name('add_to_cart');
});

Route::group(['prefix'=>'user', 'namespace'=>'Card'], function (){
    Route::get('add_to_cart/{id}', 'CardController@addToCart')->name('add_to_cart');
    Route::get('/card', 'CardController@show')->name('card');
    Route::get('/delete_card{id}', 'CardController@destroy')->name('delete_card');
    Route::get('stripe', 'CardController@stripe')->name('stripe');
    Route::post('/charge', 'CardController@charge')->name('charge');
});
