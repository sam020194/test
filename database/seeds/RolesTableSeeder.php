<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
           0=> array(
                'name' => 'super_admin',
            ),
            1=> array(
                'name' => 'admin',
            ),
            2=> array(
                'name' => 'user',
            ),
            3=> array(
                'name' => 'moderator',
            ),
            4=> array(
                'name' => 'manager',
            )
        ]);
    }
}
