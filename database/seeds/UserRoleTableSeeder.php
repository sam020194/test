<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([

            $user = new \App\User(),

            0=> array(
                $user->email = 'super.admin@mail.ru',
                $user->role_id='1',
                $user->password = bcrypt('111111'),
                $user->save(),
            ),
            1=> array(
                $user->email = 'admin@mail.ru',
                $user->role_id='2',
                $user->password = bcrypt('111111'),
                $user->save(),
            ),
            2=> array(
                $user->email = 'moderator@mail.ru',
                $user->role_id='3',
                $user->password = bcrypt('111111'),
                $user->save(),
            ),
            3=> array(
                $user->email = 'manager@mail.ru',
                $user->role_id='4',
                $user->password = bcrypt('111111'),
                $user->save(),
            ),
        ]);

    }

}
