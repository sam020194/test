@extends('user.layouts.index')
@section('content')
    @include('user.nav.nav')
    @csrf
{{--    {{dd($products)}}--}}
    <legend style="text-align: center; margin-top: 50px;">ALl PRODUCTS</legend>
    <div class="col-sm-12">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
    </div>
    <table class="table" style="text-align: center; margin-top: 50px;">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th scope="col">description</th>
            <th scope="col">Picture</th>
            <th scope="col">Buy</th>

        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->quantity}}</td>
                <td>{{$product->description}}</td>
                <td>
                    <img src="{{$product->picture->first() ? asset('picture/'.$product->picture->first()->picture) : ''}}" alt="" width="50px"; height="50px";>
                </td>
                <td scope="row">
{{--                    <a href="#" class="btn btn-danger">Buy</a>--}}
                    <p class="btn-holder"><a href="{{ route('add_to_cart', $product->id)}}" class="btn btn-warning btn-block text-center" role="button">Add to cart</a> </p>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
{{--    {{ $products->links() }}--}}

@endsection


{{--https://webmobtuts.com/backend-development/creating-a-shopping-cart-with-laravel/--}}
