@extends('user.layouts.index')
@section('content')
    @include('user.nav.nav')

    <form class="form-horizontal" action="{{route('createProduct')}}" method="post" enctype="multipart/form-data">
        @csrf
        <legend style="text-align: center; margin-top: 50px;">ADD PRODUCTS</legend>
        <div class="container" style="margin-top: 50px;">

            <!-- Form Name -->

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="product_name">PRODUCT NAME</label>
                <div class="col-md-4">
                    <input id="product_name" name="product_name" placeholder="PRODUCT NAME" class="form-control input-md" required="" type="text">
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="product_price">PRODUCT PRICE</label>
                <div class="col-md-4">
                    <input id="product_price" name="product_price" placeholder="PRODUCT PRICE" class="form-control input-md" required="" type="number">

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="available_quantity">AVAILABLE QUANTITY</label>
                <div class="col-md-4">
                    <input id="available_quantity" name="available_quantity" placeholder="AVAILABLE QUANTITY" class="form-control input-md" required="" type="number">

                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="product_description">PRODUCT DESCRIPTION</label>
                <div class="col-md-4">
                    <textarea class="form-control" id="product_description" name="product_description"></textarea>
                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="picture">PRODUCT IMAGE</label>
                        <div class="col-md-4">
                            <input id="picture" name="picture" class="input-file" type="file">
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <div class="col-md-4">
                            <button id="singlebutton" name="singlebutton" class="btn btn-primary">Save</button>
                        </div>
                    </div>

        </div>
    </form>
    <div class="col-sm-12">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
    </div>

@endsection
