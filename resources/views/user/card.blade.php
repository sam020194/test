@extends('user.layouts.index')
@section('content')
    @include('user.nav.nav')
    @csrf
{{--    {{dd($card)}}--}}

    <div class="container">
        @if(session()->get('success'))
            <div class="alert alert-success" style="text-align: center">
                {{ session()->get('success') }}
            </div>
        @endif
    </div>

    <table class="table" style="text-align: center; margin-top: 50px;">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($card as $key)
            <tr>
                <td>{{$key->name}}</td>
                <td>{{$key->price}}</td>
                <td>{{$key->quantity}}</td>
                <td scope="row">
                    <p class="btn-holder"><a href="{{ route('delete_card', $key->id)}}" class="btn btn-danger" role="button">Delete</a> </p>
                </td>
            </tr>
        @endforeach
            <tr>
                <h1 class="btn-holder"> <td><a href="{{route('stripe')}}" class="btn btn-danger">Buy all</a></td></h1>
            </tr>
        </tbody>
    </table>

@endsection
