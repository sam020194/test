@extends('user.layouts.index')
@section('content')
    @csrf
    @include('user.nav.nav')

{{--    {{dd($products)}}--}}
    <legend style="text-align: center; margin-top: 50px;">MY PRODUCTS</legend>
    <div class="container" style="margin-top: 50px;">
    @foreach($products as $product)

            <div style="display: inline-block; width: 700px; height: 600px; border: 1px solid; text-align: center; margin-top: 20px;">
{{--                <h1>name:  {{$product->name}}</h1>--}}
{{--                <h3>price:  {{ $product->price}}</h3>--}}
{{--                <h2>quantity:  {{ $product->quantity}}</h2>--}}
{{--                <p>description: <br>{{ $product->description}}</p>--}}
                {{--                <img src="{{$product->picture->first() ? asset('picture/'.$product->picture->first()->picture) : ''}}" alt="" width="150px"; height="100px";>--}}
                <div style="float: right">
                    <form action="{{ route('product.destroy', $product->id)}}" id="{{'product_delete_form_'. $product->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <div class="modal fade" id="{{'product_delete_' . $product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="color: black;">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Modal product</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" style="color: black;">
                                        Delete product?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary modalDeleteButton">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="{{'#product_delete_' . $product->id}}">Delete</button>
                </div>
{{--                <ol>--}}
{{--                    <li>--}}
{{--                        <div>--}}
{{--                            <h1>name:  {{$product->name}}</h1>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div>--}}
{{--                            <h3>price:  {{ $product->price}}</h3>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div>--}}
{{--                            <h2>quantity: {{ $product->quantity}}</h2>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div>--}}
{{--                            <p>description: <br>{{ $product->description}}</p>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div>--}}
{{--                            <img src="{{$product->picture->first() ? asset('picture/'.$product->picture->first()->picture) : ''}}" alt="" width="150px"; height="100px";>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                </ol>--}}
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">name: {{ $product->name }}</div>
                                <div class="card-body"></div>
                                <div class="card-header">price: {{ $product->price }}</div>
                                <div class="card-body"></div>
                                <div class="card-header">quantity: {{ $product->quantity }}</div>
                                <div class="card-body"></div>
                                <div class="card-header">description: {{ $product->description }}</div>
                                <div class="card-body"></div>
                                <div class="card-header">picture:
                                    <img src="{{$product->picture->first() ? asset('picture/'.$product->picture->first()->picture) : ''}}" alt="" width="150px"; height="100px";>
                                </div>
                                <div class="card-body"></div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
        <div class="col-sm-12">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
    </div>

@endsection
