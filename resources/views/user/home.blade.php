@extends('user.layouts.index')
@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>

    </style>
</head>
<body>
    @csrf
    @include('user.nav.nav')
    @if($user->role_id == 0)
        <div style="text-align: center; width: 200px; height: 200px;">
            <b><h1>{{$user->name}}</h1></b>
            <b><h1>{{$user->surname}}</h1></b>
            <b><h1>{{$user->age}}</h1></b>
        </div>
    @endif
    @if($user->role_id == 1)
        <h1 style="text-align: center">Super admin</h1>
    @endif
</body>
</html>
@endsection
