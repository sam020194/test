@extends('super_admin.layouts.index')
@section('content')
    @include('user.nav.nav')
    @csrf
    <legend style="text-align: center; margin-top: 50px;">PRODUCTS</legend>
    <table class="table" style="text-align: center; margin-top: 50px;">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th scope="col">description</th>
            <th scope="col">Picture</th>
            <th scope="col">Delete</th>

        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->quantity}}</td>
                <td>{{$product->description}}</td>
                <td>
                    <img src="{{$product->picture->first() ? asset('picture/'.$product->picture->first()->picture) : ''}}" alt="" width="50px"; height="50px";>
                </td>
                <td>
                    <form action="{{ route('destroy', $product->id)}}" id="{{'product_delete_form_'. $product->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <div class="modal fade" id="{{'product_delete_' . $product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="color: black;">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Modal product</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" style="color: black;">
                                        Delete product?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary modalDeleteButton">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="{{'#product_delete_' . $product->id}}">Delete</button>
                </td>
            </tr>
        @endforeach

        <div class="col-sm-12">
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>

        </tbody>
    </table>
{{--    {{ $products->links() }}--}}

@endsection
