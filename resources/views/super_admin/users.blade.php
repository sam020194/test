@extends('super_admin.layouts.index')
@section('content')
    @csrf
    @include('user.nav.nav')
    <div class="container" style="margin-top: 50px; text-align: center;">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Role id</th>
                <th scope="col">Name</th>
                <th scope="col">Surname</th>
                <th scope="col">Age</th>
                <th scope="col">E-mail</th>
                <th scope="col">Phone</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->role_id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->age}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>

                        <td>
                            <form action="{{ route('user.destroy', $user->id)}}" id="{{'user_delete_form_'. $user->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <div class="modal fade" id="{{'user_delete_' . $user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header" style="color: black;">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Modal users</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body" style="color: black;">
                                                Delete user?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary modalDeleteButton">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="{{'#user_delete_' . $user->id}}">Delete</button>
                            <a class="btn btn-info" href="#">Show</a>

                        </td>
                    </tr>
                @endforeach
                <div class="col-sm-12">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>
            </tbody>
        </table>
    </div>
    {{ $users->links() }}
@endsection
